declare namespace NodeJS {
  export interface ProcessEnv {
    readonly NEXT_OPENAI_API_KEY: string
  }
}
